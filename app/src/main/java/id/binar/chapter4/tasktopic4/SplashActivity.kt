package id.binar.chapter4.tasktopic4

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import id.binar.chapter4.tasktopic4.databinding.ActivitySplashBinding

@SuppressLint("CustomSplashScreen")
class SplashActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySplashBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setDelay()
    }

    private fun setDelay() {
        Handler(Looper.getMainLooper()).postDelayed({
            Intent(this@SplashActivity, LoginActivity::class.java).also {
                startActivity(it)
                finish()
            }
        }, 1000L)
    }
}