package id.binar.chapter4.tasktopic4

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences

class Authenticator(context: Context) {

    private var prefs: SharedPreferences =
        context.getSharedPreferences("auth_prefs", Context.MODE_PRIVATE)

    fun setLoggedInUser(username: String, password: String) {
        val editor = prefs.edit()
        editor.putString("key_username", username)
        editor.putString("key_password", password)
        editor.apply()
    }

    fun getLoggedInUser(): String? {
        return if (prefs.contains("key_username")) {
            prefs.getString("key_username", null)
        } else {
            clearLoggedInUser()
            null
        }
    }

    fun checkAuth(context: Context) {
        if (getLoggedInUser() != null) {
            Intent(context, HomeActivity::class.java).also {
                it.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                context.startActivity(it)
                (context as Activity).finish()
            }
        }
    }

    fun clearLoggedInUser() {
        val editor = prefs.edit()
        editor.clear()
        editor.apply()
    }
}