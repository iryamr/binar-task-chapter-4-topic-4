package id.binar.chapter4.tasktopic4

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import id.binar.chapter4.tasktopic4.databinding.ActivityLoginBinding

class LoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding

    private val authenticator: Authenticator by lazy { Authenticator(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        login()
    }

    override fun onStart() {
        super.onStart()
        authenticator.checkAuth(this)
    }

    private fun login() {
        binding.btnLogin.setOnClickListener {
            val username = binding.etUsername.text.toString()
            val password = binding.etPassword.text.toString()

            when {
                username.isEmpty() -> {
                    binding.etlUsername.error = "Username tidak boleh kosong"
                    return@setOnClickListener
                }
                password.isEmpty() -> {
                    binding.etlPassword.error = "Password tidak boleh kosong"
                    return@setOnClickListener
                }
                else -> {
                    if (username != "user") {
                        binding.etlUsername.error = "User tidak ditemukan"
                        return@setOnClickListener
                    }
                    if (password != "user") {
                        binding.etlPassword.error = "Password salah"
                        return@setOnClickListener
                    }
                }
            }

            authenticator.setLoggedInUser(username, password)
            moveToHomeActivity()
        }
    }

    private fun moveToHomeActivity() {
        Intent(this, HomeActivity::class.java).also {
            startActivity(it)
            finish()
        }
    }
}