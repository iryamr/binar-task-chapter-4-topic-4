package id.binar.chapter4.tasktopic4

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import id.binar.chapter4.tasktopic4.databinding.ActivityHomeBinding

class HomeActivity : AppCompatActivity() {

    private lateinit var binding: ActivityHomeBinding

    private val authenticator: Authenticator by lazy { Authenticator(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        logout()
    }

    private fun logout() {
        binding.btnLogout.setOnClickListener {
            authenticator.clearLoggedInUser()
            moveToLoginActivity()
        }
    }

    private fun moveToLoginActivity() {
        Intent(this, LoginActivity::class.java).also {
            it.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(it)
            finish()
        }
    }
}